const express = require('express')
const app = express()
const mysql = require('mysql');

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const connection = mysql.createConnection({
    host     : process.env.DATABASE_HOST || '127.0.0.1',
	user     : 'ynov',
	password : 'free',
	database : 'db_ynov_bootcamp',
	port: 3306,
	insecureAuth : true
});

connection.connect((err) => {
    if(err) throw err;
    console.log("Connected !");
})

app.get('/employees', function (req, res) {
    if (req.query.limit && req.query.offset) {
        connection.query(`SELECT * FROM employees LIMIT ${req.query.limit} OFFSET ${req.query.offset}`, (err, result, field) => {
            if (err) throw err
            res.json(result)
        })
    } else if (req.query.order && req.query.reverse) {
        let reverse = 'ASC';
        if (req.query.reverse === 0) {
            reverse = 'DESC'
        }
        connection.query(`SELECT * FROM employees ORDER BY ${req.query.order} ${reverse}`, (err, result, field) => {
            if (err) throw err
            res.json(result)
        })
    } else {
        connection.query("SELECT * FROM employees",  (err, result, fields) => {
            if (err) throw err;
            res.json(result)
        })
    }
})

app.get('/employees/:id', function (req, res) {
    connection.query(`SELECT * FROM employees WHERE employee_id = ${req.params.id}`, (err, result, fields) => {
        if (err) throw err
        res.json(result)
    })
})

app.delete('/employees/:id', function (req, res) {
    connection.query(`DELETE FROM employees WHERE employee_id = ${req.params.id}`, (err, result, fields) => {
        if (err) throw err
        res.json(result)
    })
})

app.post(`/employees`, function (req, res) {
    let body = req.body
    connection.query(`INSERT INTO employees(first_name,last_name,email,phone_number,hire_date,job_id,salary,manager_id,department_id) VALUES ('${body.first_name}','${body.last_name}','${body.email}','${body.phone_number}','${body.hire_date}','${body.job_id}','${body.salary}','${body.manager_id}','${body.department_id}')`, (err, result, fields) => {
        if (err) throw err
        res.json(result)
    })
})

app.put(`/employees/:id`, function (req, res) {
    let body = req.body
    connection.query(`UPDATE employees SET first_name = '${body.first_name}', last_name = '${body.last_name}' WHERE employee_id = ${req.params.id}`, (err, result, fields) => {
        if (err) throw err
        res.json(result)
    })
})

app.listen(3000)

